package home5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {
	
	public Date startTime;
	public Date endTime;
	public String activity;
	
	public MonitoredData(Date startTime, Date endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	public static void main( String[] args ) throws ParseException, FileNotFoundException
    {
    	String fileName = "D:\\PT2018\\PT2018_30424_Miron_Cristian\\Assignment5\\Activities.txt";
		List<String> list = new ArrayList<>();
		List<MonitoredData> monitoredDataList = new ArrayList<>();
		Methods methods = new Methods();
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			list = stream.collect(Collectors.toList());
			monitoredDataList = methods.convertToMonitoredDataObjects(list, monitoredDataList);
			//int differentDaysCount = methods.daysCount(monitoredDataList);
			//System.out.println(differentDaysCount);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//monitoredDataList.forEach(System.out::println);
		
		//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	List<Integer> dayList = new ArrayList<>();
    	for(MonitoredData monitoredData : monitoredDataList) {
    		Date date = monitoredData.getStartTime();
    		String dayString = sdf.format(date);
    		String[] firstSplit = dayString.split(" ");
    		String firstPartAfterFirstSplit = firstSplit[0];
    		String[] secondSplit = firstPartAfterFirstSplit.split("-");
    		String day = secondSplit[2];
    		int aux = Integer.parseInt(day);
    		if (methods.verifyDay(aux,dayList)) {
    			dayList.add(aux);
    		}
    	}
		//int nrDays = (int) monitoredDataList.stream().map(x->x.startTime.getDate()).distinct().count();
		//System.out.println(nrDays);
		List<Integer> numberOfDays = monitoredDataList
				.stream()
				.map(x->x.startTime.getDate())
				.distinct()
                .collect(Collectors.toList());
		//numberOfDays.forEach(System.out::println);
		System.out.println("Numarul de zile diferite este: "+numberOfDays.size());
		
		//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		Map<String, Long> numberOfActivities = new HashMap<>();
		numberOfActivities = monitoredDataList
				.stream()
				.collect(Collectors.groupingBy(x -> x.activity, Collectors.counting()));
		File file = new File("NumberOfActivities.txt");
		FileOutputStream fos = new FileOutputStream(file);
		PrintStream ps = new PrintStream(fos);
		PrintStream console = System.out;
		System.setOut(ps);
		Map<String,Long> auxMap = numberOfActivities;
		numberOfActivities.forEach((activity,counter)->System.out.println(activity +"  "+counter));
		System.setOut(console);
		
		//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		Map<Object, Map<Object, Long>> numberOfActivitiesPerDay = new HashMap<>();
		numberOfActivitiesPerDay = monitoredDataList.stream()
				.collect(Collectors.groupingBy(q->q.startTime.getDate(), Collectors.groupingBy(x -> x.activity, Collectors.counting())));
		File NumberOfActivities = new File("NumberOfActivitiesPerDay.txt");
		FileOutputStream NumberOfActivitiesFos = new FileOutputStream(NumberOfActivities);
		PrintStream NumberOfActivitiesPS = new PrintStream(NumberOfActivitiesFos);
		System.setOut(NumberOfActivitiesPS);
		numberOfActivitiesPerDay.forEach((day,activityList)->System.out.println(day +" " +activityList));
		System.setOut(console);
		
		//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		File tenHOrMore = new File("TenHOrMore.txt");
		FileOutputStream tenHOrMoreFos = new FileOutputStream(tenHOrMore);
		PrintStream tenHOrMorePS = new PrintStream(tenHOrMoreFos);
		System.setOut(tenHOrMorePS);
		Map<String, Integer> activityDurationMap = new HashMap<>();
		activityDurationMap = monitoredDataList.stream()
				.collect(Collectors.groupingBy(x->x.activity,Collectors.summingInt(x->(int)x.getSum(x))));
				activityDurationMap.forEach((activity,date)->{
					if(date>=10) {
					System.out.println(activity+"    "+date);
					}});
		System.setOut(console);
		
		//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		File less = new File("LessThan5.txt");
		FileOutputStream lessFos = new FileOutputStream(less);
		PrintStream lessPS = new PrintStream(lessFos);
		System.setOut(lessPS);
		Map<String,Integer>activityDurationLessThan5 = new HashMap<>();
		activityDurationLessThan5 = monitoredDataList.stream()
				.collect(Collectors.groupingBy(x->x.activity,Collectors.summingInt(x->x.sumForLessThan5(x))));
		List<String> toPrintList = new ArrayList<>();
		activityDurationLessThan5.forEach((a,b)->{
			for(Entry<String, Long> verify : auxMap.entrySet()) {
				if (a == verify.getKey()) {
					long aux = (verify.getValue()*90)/100;
					if(b>aux) {
						toPrintList.add(a);
					}
				}
			}
		});
		System.out.println(toPrintList.toString());
		System.setOut(console);
		
		//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		System.out.println("");
		System.out.println("FINISH! Loved this subject <3 !!");
		
		//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }
	public Date getStartTime() {
		return startTime;
	}
	public int sumForLessThan5(MonitoredData monD) {
		long diff =TimeUnit.MILLISECONDS.toMinutes(monD.endTime.getTime() - monD.startTime.getTime());
		if (diff<5) {
			return 1;
		}else {
		return 0;
		}
	}
	public long getDifference(MonitoredData monitoredData) {
		long diff =TimeUnit.MILLISECONDS.toHours(monitoredData.endTime.getTime() - monitoredData.startTime.getTime());
		return diff;
	}
	public long getSum (MonitoredData monD) {
		long diff =monD.getEndTime().getTime() - monD.getStartTime().getTime();
		long diffDays = diff / (60 * 60 * 1000);
		return diffDays;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public int returnDay() {
		return this.startTime.getDay();
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	@Override
	public String toString() {
		return "StartTime = " + this.startTime + ", endTime = " + this.endTime + ", activity = " + this.activity;
	}
}