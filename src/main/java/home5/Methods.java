package home5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Methods {

	public Methods() {
		
	}
    public List<MonitoredData> convertToMonitoredDataObjects(List<String> list,List<MonitoredData> monitoredDataList) throws ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(String s: list) {
			//s.trim();
			String[] parts = s.split("		");
			String part1 = parts[0];
			String part2 = parts[1];
			Date startDateConverted = sdf.parse(part1.trim());
			String part3 = parts[2];
			Date endDateConverted = sdf.parse(part2.trim());
			MonitoredData monitoredDataToWorkWith = new MonitoredData(startDateConverted,endDateConverted,part3.trim());
			monitoredDataList.add(monitoredDataToWorkWith);
		}
		return monitoredDataList;
    }
    
    public int daysCount(List<MonitoredData> monitoredDataList) {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	int count = 0;	
    	List<Integer> dayList = new ArrayList<>();
    	for(MonitoredData monitoredData : monitoredDataList) {
    		Date date = monitoredData.getStartTime();
    		String dayString = sdf.format(date);
    		String[] firstSplit = dayString.split(" ");
    		String firstPartAfterFirstSplit = firstSplit[0];
    		String[] secondSplit = firstPartAfterFirstSplit.split("-");
    		String day = secondSplit[2];
    		int aux = Integer.parseInt(day);
    		if (verifyDay(aux,dayList)) {
    			dayList.add(aux);
    		}
    	}
    	for(Integer day: dayList) {
    		count++;
    	}
		return count;
    }
    
    public boolean verifyDay(int day,List<Integer> dayList) {
    	boolean verification = true;
    	for(Integer aux : dayList) {
    		if (aux == day) {
    			verification = false;
    		}
    	}
		return verification;
    }
	
}
